import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/chat",
    name: "chat",
    component: () => import(/* webpackChunkName: "about" */ "../views/Chat.vue")
  },
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName: "about" */ "../views/Home.vue")
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/quiz",
    name: "quiz",
    component: () => import(/* webpackChunkName: "about" */ "../views/Quiz.vue")
  },
  {
    path: "/list",
    name: "quizlist",
    component: () => import(/* webpackChunkName: "about" */ "../views/QuizList.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
